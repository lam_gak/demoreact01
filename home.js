'use strict';
import React, { Component } from 'react';
import {
  AppRegistry,
  View,
  TouchableHighlight,
  Text
} from 'react-native';



export default class home extends Component {

  render(){
    return (
      <View>
        <Text>
               Home page!
        </Text>
        <Text>
               Welcom to React-native!
        </Text>
        <TouchableHighlight onPress={() => {
              this.props.navigator.push({id: "Login"});
          }}>
          <Text>Logout</Text>
        </TouchableHighlight>

      </View>
    );
  }  
}


AppRegistry.registerComponent('home', () => home);