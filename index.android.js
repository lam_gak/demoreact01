/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 * https://facebook.github.io/react-native/docs/navigator.html
 * https://medium.com/@dabit3/react-native-navigator-navigating-like-a-pro-in-react-native-3cb1b6dc1e30#.klv96r43n
 */

import React, { Component } from 'react';
import { TouchableHighlight,AppRegistry,View,Text, Navigator } from 'react-native';
import Login from './login';
import Signup from './signup';
import Home from './home';


export default class demo01 extends Component {
  render() {
    return (
          <Navigator initialRoute={{id:'Login'}}
          renderScene = {this._renderScene}
        />
      );
  }

  //define route
  _renderScene(route,navigator) {
    switch (route.id) {
    case 'Login':
      return <Login navigator={navigator} title="Login"/>;
    case 'Signup':
      return <Signup navigator={navigator} title="Signup"/>;
    case 'Home':
      return <Home navigator={navigator} title="Home"/>;
    default:
      console.error('Encountered unexpected route: ' + route.id);
    }
  }

}
AppRegistry.registerComponent('demo01', () => demo01);
