'use strict';
import React, { Component } from 'react';
import {
  AppRegistry,
  View,
  TextInput,
  TouchableHighlight,
  Navigator,
  Text
} from 'react-native';

import Signup from './signup';
import Home from './home';


export default class login extends Component {

  render(){
    return (
      <View>
        <Text>
               Login page!
        </Text>
        <TextInput style={{width: 300}} placeholder="Email"/>
        <TextInput style={{width: 300}} placeholder="Password" secureTextEntry={true}/>

        <TouchableHighlight onPress={() => {
              this.props.navigator.push({id: 'Home'});
          }}>
          <Text>Login</Text>
        </TouchableHighlight>

          <TouchableHighlight onPress={() => {
                this.props.navigator.push({id: "Signup"});
            }}>
            <Text>Signup</Text>
          </TouchableHighlight>

      </View>
    );
  }

  // signup(){
  //     this.props.navigator.push({
  //       component: Signup
  //     });

  // } 
}


AppRegistry.registerComponent('login', () => login);