'use strict';
import React, { Component } from 'react';
import {
  AppRegistry,
  View,
  TextInput,
  TouchableHighlight,
  Text
} from 'react-native';



export default class signup extends Component {

  render(){
    return (
      <View>
        <Text>
               signup page
        </Text>
        <TextInput style={{width: 300}} placeholder="Email"/>
        <TextInput style={{width: 300}} placeholder="Password" secureTextEntry={true}/>
        <TouchableHighlight onPress={() => {
              this.props.navigator.push({id: "Login"});
          }}>
          <Text>Sign up</Text>
        </TouchableHighlight>

      </View>
    );
  }  
}


AppRegistry.registerComponent('signup', () => signup);